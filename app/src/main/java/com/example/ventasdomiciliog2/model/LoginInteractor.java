package com.example.ventasdomiciliog2.model;

import com.example.ventasdomiciliog2.mvp.LoginMVP;

import java.util.HashMap;
import java.util.Map;

public class LoginInteractor implements LoginMVP.Model {

    private Map<String, String> users;

    public LoginInteractor() {
        users = new HashMap<>();
        users.put("cdiaz@email.com", "12345678");
        users.put("test@email.com", "87654321");
    }

    @Override
    public void validateCredentials(String email, String password,
                                    ValidateCredentialsCallback callback) {

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (users.get(email) != null
                && users.get(email).equals(password)) {
            callback.onSuccess();
        } else {
            callback.onFailure();
        }
    }
}
