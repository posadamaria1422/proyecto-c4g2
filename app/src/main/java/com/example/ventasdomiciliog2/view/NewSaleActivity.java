package com.example.ventasdomiciliog2.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;

import com.example.ventasdomiciliog2.R;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class NewSaleActivity extends AppCompatActivity {

    private TextInputLayout tilClient;
    private TextInputEditText etClient;
    private TextInputLayout tilAddress;
    private TextInputEditText etAddress;
    private TextInputLayout tilAmount;
    private TextInputEditText etAmount;
    private TextInputLayout tilNumber;
    private TextInputEditText etNumber;
    private TextInputLayout tilPeriodicity;
    private MaterialAutoCompleteTextView etPeriodicity;
    private TextInputLayout tilPart;
    private TextInputEditText etPart;
    private TextInputLayout tilDate;

    private Date selectedDate;
    private TextInputEditText etDate;

    private AppCompatButton btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sale);

        initUI();
    }

    private void initUI() {
        tilClient = findViewById(R.id.til_client);
        etClient = findViewById(R.id.et_client);

        tilAddress = findViewById(R.id.til_address);
        etAddress = findViewById(R.id.et_address);

        tilAmount = findViewById(R.id.til_amount);
        etAmount = findViewById(R.id.et_amount);

        tilNumber= findViewById(R.id.til_number);
        etNumber = findViewById(R.id.et_number);

        tilPeriodicity = findViewById(R.id.til_periodicity);
        etPeriodicity = findViewById(R.id.et_periodicity);

        tilPart = findViewById(R.id.til_part);
        etPart = findViewById(R.id.et_part);

        tilDate = findViewById(R.id.til_date);
        tilDate.setEndIconOnClickListener(V -> onDateClick());

        etDate = findViewById(R.id.et_date);

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(v -> onSaveClick());
    }

    private void onDateClick() {
        long today = MaterialDatePicker.todayInUtcMilliseconds();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.add(Calendar.DAY_OF_MONTH, 1);

        CalendarConstraints constraint = new CalendarConstraints.Builder()
                .setValidator(DateValidatorPointForward.from(calendar.getTimeInMillis()))
                .build();

        MaterialDatePicker<Long> datePicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText(R.string.newsale_date)
                .setSelection(calendar.getTimeInMillis())
                .setCalendarConstraints(constraint)
                .build();
        datePicker.addOnPositiveButtonClickListener(this::setSelectedDate);

        datePicker.show(getSupportFragmentManager(), "date");
    }

    private void setSelectedDate(Long selection) {
        selectedDate = new Date(selection);
        etDate.setText(SimpleDateFormat.getDateInstance().format(selectedDate));
    }

    private void onSaveClick(){
        startActivity(new Intent(NewSaleActivity.this, PaymentsActivity.class));
    }
}