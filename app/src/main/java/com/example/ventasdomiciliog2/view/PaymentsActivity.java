package com.example.ventasdomiciliog2.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.ventasdomiciliog2.R;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

public class PaymentsActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private MaterialToolbar toolbar;
    private NavigationView navigationView;

    private RecyclerView rvPayments;
    private FloatingActionButton btnNewSale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        initUI();
    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);

        toolbar = findViewById(R.id.app_toolbar);
        toolbar.setNavigationOnClickListener(v -> drawerLayout.openDrawer(navigationView));

        navigationView =findViewById(R.id.nv_payments);
        navigationView.setNavigationItemSelectedListener(this :: onMenuItemClick);

        rvPayments = findViewById(R.id.rv_payments);

        btnNewSale = findViewById(R.id.btn_new_sale);
        btnNewSale.setOnClickListener(v -> onNewSaleClick());
    }

    private boolean onMenuItemClick(MenuItem menuItem){
        menuItem.setChecked(true);
        drawerLayout.closeDrawers();
        return true;
    }

    private void onNewSaleClick(){
        startActivity(new Intent(PaymentsActivity.this, NewSaleActivity.class));
    }
}